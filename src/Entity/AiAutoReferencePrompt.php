<?php

namespace Drupal\ai_auto_reference\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the AI Auto-reference Prompt entity.
 *
 * @ConfigEntityType(
 *   id = "ai_auto_reference_prompt",
 *   label = @Translation("AI Auto-reference Prompt"),
 *   handlers = {
 *     "list_builder" = "Drupal\ai_auto_reference\AiAutoReferencePromptListBuilder",
 *     "form" = {
 *       "add" = "Drupal\ai_auto_reference\Form\AiAutoReferencePromptForm",
 *       "edit" = "Drupal\ai_auto_reference\Form\AiAutoReferencePromptForm",
 *       "delete" = "Drupal\ai_auto_reference\Form\AiAutoReferencePromptDeleteForm"
 *     }
 *   },
 *   config_prefix = "ai_auto_reference_prompt",
 *   admin_permission = "administer ai autoreference",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "prompt"
 *   },
 *   links = {
 *     "edit-form" = "/admin/content/ai-autoreference-settings/{ai_auto_reference_prompt}",
 *     "delete-form" = "/admin/content/ai-autoreference-settings/{ai_auto_reference_prompt}/delete"
 *   }
 * )
 */
class AiAutoReferencePrompt extends ConfigEntityBase implements AiAutoReferencePromptInterface {

  /**
   * The AI Auto-reference Prompt ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The AI Auto-reference Prompt label.
   *
   * @var string
   */
  protected $label;

  /**
   * The AI Auto-reference Prompt text.
   *
   * @var string
   */
  protected $prompt;

  /**
   * {@inheritdoc}
   */
  public function getPrompt(): string {
    return $this->prompt ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function setPrompt($prompt): void {
    $this->prompt = $prompt;
  }

}

<?php

namespace Drupal\ai_auto_reference\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an AI Auto-reference Prompt entity.
 */
interface AiAutoReferencePromptInterface extends ConfigEntityInterface {

  /**
   * Gets the prompt text.
   *
   * @return string
   *   The prompt text.
   */
  public function getPrompt(): string;

  /**
   * Sets the prompt text.
   *
   * @param string $prompt
   *   The prompt text.
   */
  public function setPrompt($prompt): void;

}

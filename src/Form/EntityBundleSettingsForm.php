<?php

namespace Drupal\ai_auto_reference\Form;

use Drupal\Core\Entity\Display\EntityFormDisplayInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\field\Entity\FieldConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A form with a list of AI autoreferences for an entity type.
 *
 * @package Drupal\conditional_fields\Form
 */
class EntityBundleSettingsForm extends FormBase {

  /**
   * The route name for an AI autoreference edit form.
   *
   * @var string
   */
  protected $editPath = 'ai_auto_reference.edit_form';

  /**
   * The route name for AI autoreference delete form.
   *
   * @var string
   */
  protected $deletePath = 'ai_auto_reference.delete_form';

  /**
   * Provides an interface for an entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Provides an interface for entity type managers.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * Name of the entity type being configured.
   *
   * @var string
   */
  protected $entityType;

  /**
   * Name of the entity bundle being configured.
   *
   * @var string
   */
  protected $bundleName;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Provides an interface for an entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Provides an interface for entity type managers.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   */
  public function __construct(
    EntityFieldManagerInterface $entity_field_manager,
    EntityTypeManagerInterface $entity_type_manager,
    EntityDisplayRepositoryInterface $entity_display_repository,
  ) {
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ai_auto_reference_node_bundle_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $node_type = NULL) {
    $this->entityType = 'node';
    $this->bundleName = $node_type;

    $form['entity_type'] = [
      '#type' => 'hidden',
      '#value' => $this->entityType,
    ];
    $form['bundle'] = [
      '#type' => 'hidden',
      '#value' => $this->bundleName,
    ];

    return $this->buildTable($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $table = $form_state->getValue('table');

    $entity_form_display = $this->entityTypeManager
      ->getStorage('entity_form_display')
      ->load($this->entityType . '.' . $this->bundleName . '.' . 'default');

    if (!$entity_form_display instanceof EntityFormDisplayInterface) {
      return;
    }

    $field_name = $table['add_new_autoreference']['field_name'];
    $view_mode = $table['add_new_autoreference']['view_mode'];
    $selected_prompt = $table['add_new_autoreference']['prompt'];

    // Store the selected prompt along with the field and view mode.
    $entity_form_display->setThirdPartySetting('ai_auto_reference', $field_name, [
      'view_mode' => $view_mode,
      'prompt' => $selected_prompt,
    ]);

    $entity_form_display->save();
  }

  /**
   * Builds table with conditional fields.
   *
   * @param array $form
   *   The form render array.
   *
   * @return array
   *   The updated form.
   */
  protected function buildTable(array $form): array {
    // Preparing table skeleton.
    $table = [
      '#type' => 'table',
      '#entity_type' => $this->entityType,
      '#bundle_name' => $this->bundleName,
      '#header' => [
        $this->t('Target field'),
        $this->t('View mode'),
        $this->t('Prompt'),
        ['data' => $this->t('Operations'), 'colspan' => 2],
      ],
    ];

    // Getting entity reference fields for current bundle.
    $bundle_fields = $this->entityFieldManager->getFieldDefinitions($this->entityType, $this->bundleName);
    $reference_fields = [];
    foreach ($bundle_fields as $field_name => $bundle_field) {
      if ($bundle_field->getType() == 'entity_reference'
        && $bundle_field instanceof FieldConfig
      ) {
        $field_settings = $bundle_field->getSettings();
        // Target entity type should be node or taxonomy term.
        if (!empty($field_settings['target_type'])
          && in_array($field_settings['target_type'], ['node', 'taxonomy_term'])
        ) {
          $reference_fields[$field_name] = $bundle_field->label();
        }
      }
    }

    /** @var \Drupal\Core\Entity\Display\EntityDisplayInterface $form_display_entity */
    $form_display_entity = $this->entityTypeManager
      ->getStorage('entity_form_display')
      ->load("$this->entityType.$this->bundleName.default");
    if (!$form_display_entity) {
      $form['ai_reference_wrapper']['table'] = $table;
      return $form;
    }

    $view_modes = $this->entityDisplayRepository->getViewModeOptionsByBundle($this->entityType, $this->bundleName);

    // Fetch available AI Auto-reference Prompts.
    $prompt_options = [];
    $prompt_entities = $this->entityTypeManager->getStorage('ai_auto_reference_prompt')->loadMultiple();
    foreach ($prompt_entities as $prompt) {
      $prompt_options[$prompt->id()] = $prompt->label();
    }

    // Displaying already added AI auto-reference blocks.
    $settings = $form_display_entity->getThirdPartySettings('ai_auto_reference');
    foreach ($reference_fields as $field_name => $label) {
      if (empty($settings[$field_name]['view_mode']) || empty($settings[$field_name]['prompt'])) {
        continue;
      }

      $parameters = [
        'node_type' => $this->bundleName,
        'field_name' => $field_name,
      ];

      // Get stored prompt label.
      $stored_prompt_label = $prompt_options[$settings[$field_name]['prompt']] ?? $this->t('Unknown Prompt');

      $table[] = [
        'field_name' => ['#markup' => "{$label} ({$field_name})"],
        'view_mode' => ['#markup' => $view_modes[$settings[$field_name]['view_mode']]],
        'prompt' => ['#markup' => $stored_prompt_label],
        'actions' => [
          '#type' => 'operations',
          '#links' => [
            'edit' => [
              'title' => $this->t('Edit'),
              'url' => Url::fromRoute($this->editPath, $parameters),
            ],
            'delete' => [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute($this->deletePath, $parameters),
            ],
          ],
        ],
      ];
      // Only allow one configuration per field.
      unset($reference_fields[$field_name]);
    }

    // Add new AI Auto-reference row.
    $table['add_new_autoreference'] = [
      'field_name' => [
        '#type' => 'select',
        '#title' => $this->t('Target field'),
        '#title_display' => 'invisible',
        '#description' => $this->t('Target field'),
        '#options' => $reference_fields,
        '#prefix' => '<div class="add-new-placeholder">' . $this->t('Add new autoreference') . '</div>',
        '#required' => TRUE,
        '#attributes' => [
          'class' => ['conditional-fields-selector'],
          'style' => ['resize: both;'],
        ],
      ],
      'view_mode' => [
        '#type' => 'select',
        '#title' => $this->t('View mode'),
        '#title_display' => 'invisible',
        '#description' => $this->t('View mode'),
        '#options' => $view_modes,
        '#prefix' => '<div class="add-new-placeholder">&nbsp;</div>',
        '#required' => TRUE,
        '#attributes' => ['class' => ['conditional-fields-selector']],
      ],
      'prompt' => [
        '#type' => 'select',
        '#title' => $this->t('Prompt'),
        '#title_display' => 'invisible',
        '#description' => $this->t('Select a predefined AI Auto-reference Prompt'),
        '#options' => $prompt_options,
        '#prefix' => '<div class="add-new-placeholder">&nbsp;</div>',
        '#required' => TRUE,
        '#attributes' => ['class' => ['conditional-fields-selector']],
      ],
      'actions' => [
        'submit' => [
          '#type' => 'submit',
          '#value' => $this->t('Add autoreference'),
        ],
      ],
    ];

    $form['ai_reference_wrapper']['table'] = $table;
    return $form;
  }

}

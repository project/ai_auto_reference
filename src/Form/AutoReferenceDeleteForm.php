<?php

namespace Drupal\ai_auto_reference\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A form to delete AI autoreference configuration.
 *
 * @package Drupal\ai_auto_reference\Form
 */
class AutoReferenceDeleteForm extends ConfirmFormBase {

  /**
   * The node type this AI autoreference configuration is attached to.
   *
   * @var string
   */
  protected $nodeType;

  /**
   * The name of the field to delete AI autoreference configuration for.
   *
   * @var string
   */
  protected $fieldName;

  /**
   * Provides an interface for entity type managers.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new AutoReferenceDeleteForm.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Provides an interface for entity type managers.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $node_type = NULL, $field_name = NULL) {
    $this->nodeType = $node_type;
    $this->fieldName = $field_name;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the %field_name AI autoreference configuration?', [
      '%field_name' => $this->fieldName,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('ai_auto_reference.node_bundle.settings', [
      'node_type' => $this->nodeType,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ai_auto_reference_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (empty($this->nodeType) || empty($this->fieldName)) {
      return;
    }

    /** @var \Drupal\Core\Entity\Display\EntityDisplayInterface $form_display_entity */
    $form_display_entity = $this->entityTypeManager
      ->getStorage('entity_form_display')
      ->load('node.' . $this->nodeType . '.default');

    if (!$form_display_entity) {
      return;
    }

    $form_display_entity->unsetThirdPartySetting('ai_auto_reference', $this->fieldName);
    $form_display_entity->save();
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}

<?php

namespace Drupal\ai_auto_reference\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for AI Auto-reference Prompt add and edit forms.
 */
class AiAutoReferencePromptForm extends EntityForm {

  /**
   * Constructs an AiAutoReferencePromptForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $ai_auto_reference_prompt = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $ai_auto_reference_prompt->label(),
      '#description' => $this->t('Label for the AI Auto-reference Prompt.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $ai_auto_reference_prompt->id(),
      '#machine_name' => [
        'exists' => [$this, 'exists'],
      ],
      '#disabled' => !$ai_auto_reference_prompt->isNew(),
    ];

    $form['prompt'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Prompt'),
      '#default_value' => $ai_auto_reference_prompt->getPrompt(),
      '#description' => $this->t('Enter the prompt text. It must contain {CONTENTS} and {POSSIBLE_RESULTS}.'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $prompt = $form_state->getValue('prompt');
    if (!str_contains($prompt, '{CONTENTS}') || !str_contains($prompt, '{POSSIBLE_RESULTS}')) {
      $form_state->setErrorByName('prompt', $this->t('The prompt must contain both {CONTENTS} and {POSSIBLE_RESULTS}.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $ai_auto_reference_prompt = $this->entity;
    $status = $ai_auto_reference_prompt->save();

    if ($status === SAVED_NEW) {
      $this->messenger()->addMessage($this->t('The %label AI Auto-reference Prompt has been created.', [
        '%label' => $ai_auto_reference_prompt->label(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('The %label AI Auto-reference Prompt has been updated.', [
        '%label' => $ai_auto_reference_prompt->label(),
      ]));
    }

    $form_state->setRedirect('entity.ai_auto_reference_prompt.collection');
    return $status;
  }

  /**
   * Helper function to check whether an AI Auto-reference Prompt entity exists.
   */
  public function exists($id) {
    $entity = $this->entityTypeManager->getStorage('ai_auto_reference_prompt')->getQuery()
      ->condition('id', $id)
      ->accessCheck()
      ->execute();
    return (bool) $entity;
  }

}

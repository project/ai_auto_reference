<?php

namespace Drupal\ai_auto_reference\Form;

use Drupal\ai\AiProviderPluginManager;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The AI Autoreference configurable settings.
 *
 * @package Drupal\ai_auto_reference\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config manager.
   * @param \Drupal\ai\AiProviderPluginManager $aiProviderManager
   *   The AI provider manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typedConfigManager,
    protected AiProviderPluginManager $aiProviderManager,
  ) {
    parent::__construct($config_factory, $typedConfigManager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('ai.provider'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ai_auto_reference.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ai_auto_reference_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ai_auto_reference.settings');
    $form['provider'] = [
      '#type' => 'select',
      '#title' => $this->t('AI provider'),
      '#options' => $this->aiProviderManager->getSimpleProviderModelOptions('chat'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['provider'] ?? $this->aiProviderManager->getSimpleDefaultProviderOptions('chat'),
      '#description' => $this->t('Select which provider to use for this plugin. See the <a href=":link">Provider overview</a> for details about each provider.', [
        ':link' => '/admin/config/ai/providers',
      ]),
    ];
    $form['token_limit'] = [
      '#type' => 'number',
      '#step' => 1,
      '#title' => $this->t('Token Limit'),
      '#description' => $this->t('The number of tokens to limit API calls to.'),
      '#default_value' => $config->get('token_limit'),
    ];
    $form['auto_apply_suggestions'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Auto-apply suggestions'),
      '#description' => $this->t('By default, editors will be shown a screen with the suggested relationships, where suggestions can be optionally skipped then saved. Check this box to automatically accept the suggestions from the AI.'),
      '#default_value' => $config->get('auto_apply_suggestions'),
    ];
    $form['auto_apply_relevance_levels'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Auto-apply relevance levels'),
      '#description' => $this->t('By default, editors will be shown a screen with the suggested relationships, where suggestions can be optionally skipped then saved. Select which  relevance levels you would like to automatically apply.'),
      '#options' => [
        'high' => $this->t('High Relevance'),
        'medium' => $this->t('Low Relevance'),
      ],
      '#multiple' => TRUE,
      '#default_value' => $config->get('auto_apply_relevance_levels'),
      '#states' => [
        'visible' => [
          ':input[name="auto_apply_suggestions"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="auto_apply_suggestions"]' => ['checked' => TRUE],
        ],
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Ensure at least one of relevance level is applied when
    // auto apply suggestions is in place.
    if (
      $form_state->getValue('auto_apply_suggestions')
      && !array_filter($form_state->getValue('auto_apply_relevance_levels'))
    ) {
      $message = $this->t('At least one level of relevance should be auto-applied.');
      $form_state->setErrorByName('auto_apply_relevance_levels', $message);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $relevance_levels = array_filter($form_state->getValue('auto_apply_relevance_levels'));
    $config = $this->config('ai_auto_reference.settings')
      ->set('provider', $form_state->getValue('provider'))
      ->set('token_limit', (int) $form_state->getValue('token_limit'))
      ->set('auto_apply_suggestions', $form_state->getValue('auto_apply_suggestions'))
      ->set('auto_apply_relevance_levels', array_keys($relevance_levels));
    $config->save();
  }

}

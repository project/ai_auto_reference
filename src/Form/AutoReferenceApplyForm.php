<?php

namespace Drupal\ai_auto_reference\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * An auto-reference apply form.
 *
 * @package Drupal\ai_auto_reference\Form
 */
class AutoReferenceApplyForm extends FormBase {

  /**
   * Provides an interface for entity type managers.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Class constructor.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    RequestStack $request_stack,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ai_reference_apply_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\node\NodeInterface $node
   *   Node object.
   * @param array $configuration
   *   AU autoreference configuration for node bundle.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?NodeInterface $node = NULL, ?array $configuration = NULL) {
    $form = [];
    $query = $this->requestStack->getCurrentRequest()->query->all();
    $auto_apply = !empty($query['auto-apply']);
    if (empty($query) || empty($configuration) || empty($node)) {
      return $form;
    }

    // Add CSS.
    $form['#attached']['library'][] = 'ai_auto_reference/ai-auto-reference-admin';

    $field_definitions = (array) $node->getFieldDefinitions();

    foreach ($configuration as $ai_field_config) {
      if (!empty($ai_field_config['field_name']) && !empty($query[$ai_field_config['field_name']])) {
        if (!isset($form['container'])) {
          $form['container'] = [
            '#type' => 'fieldset',
            '#collpsible' => FALSE,
            '#title' => $this->t('The following relationships have been suggested by the AI tool:'),
            '#attributes' => [
              'class' => [
                'fieldset__ai-auto-reference',
              ],
            ],
          ];
          if ($auto_apply) {
            $form['container']['#title'] = $this->t('The following relationships have been applied by the AI tool:');
            $form['container']['#description'] = $this->t('The above suggestions have been already applied.');
          }
        }
        $empty_results = TRUE;
        foreach (['h', 'm'] as $relevancy_level) {
          $relevancy = $relevancy_level == 'h' ? 'High' : 'Medium';
          if (!empty($query[$ai_field_config['field_name']][$relevancy_level])) {
            $empty_results = FALSE;
            $ids = explode(',', $query[$ai_field_config['field_name']][$relevancy_level]);
            $settings = $field_definitions[$ai_field_config['field_name']]->getSettings();
            $target_entity_type = str_replace('default:', '', $settings['handler']);
            $storage = $this->entityTypeManager->getStorage($target_entity_type);
            $entities = $storage->loadMultiple($ids);
            $options = [];
            foreach ($entities as $entity) {
              $options[$entity->id()] = $entity->label();
            }
            $form['container'][$ai_field_config['field_name']][$relevancy_level] = [
              '#type' => 'checkboxes',
              // Disable checkboxes in auto-apply case.
              '#disabled' => $auto_apply,
              '#options' => $options,
              '#default_value' => $relevancy_level == 'h' ? $ids : [],
              '#title' => $this->t('%relevancy relevance suggestions for field %field', [
                '%field' => $field_definitions[$ai_field_config['field_name']]->getLabel(),
                '%relevancy' => $relevancy,
              ]),
            ];
          }
        }
        // Empty results case.
        if ($empty_results) {
          $form['container'] = [
            '#type' => 'item',
            '#markup' => $this->t('No relationships have been suggested. You could reconfigure and/or try again.'),
          ];
        }
      }
    }

    // No sense in submit button for auto-apply case.
    // Check if node id exists (in case we add a new node)
    if (!$auto_apply && $node->id()) {
      $submit = [
        '#type' => 'submit',
        '#value' => $this->t('Save selections'),
        '#weight' => 50,
        '#suffix' => Link::fromTextAndUrl(
          $this->t('Reject all suggestions'),
          $node->toUrl('edit-form', ['nid' => $node->id()])
        )->toString(),
      ];
      if (isset($form['container']['#type']) && $form['container']['#type'] === 'fieldset') {
        $form['container']['submit'] = $submit;
      }
      else {
        $form['submit'] = $submit;
      }
    }

    $form['#tree'] = TRUE;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $build_info = $form_state->getBuildInfo();
    if (!empty($build_info['args'][0]) && $build_info['args'][0] instanceof NodeInterface) {
      $node = $build_info['args'][0];
      foreach ($values['container'] as $field_name => $relevancy_values) {
        foreach ($relevancy_values as $relevancy_value_ids) {
          foreach ($relevancy_value_ids as $id) {
            if ($id) {
              $node->{$field_name}[] = ['target_id' => $id];
            }
          }
        }
      }
      $node->save();
      $form_state->setRedirectUrl($node->toUrl('edit-form', ['nid' => $node->id()]));
    }
  }

}

<?php

namespace Drupal\ai_auto_reference\Form;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * An edit form for AI autoreference item.
 *
 * @package Drupal\ai_auto_reference\Form
 */
class AutoReferenceEditForm extends FormBase {

  /**
   * The name of the route to redirect to when the form has been submitted.
   *
   * @var string
   */
  protected $redirectPath = 'ai_auto_reference.node_bundle.settings';

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * Provides an interface for entity type managers.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Provides an interface for an entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity type machine name, eg 'node'.
   *
   * @var string
   */
  protected $entityType;

  /**
   * Class constructor.
   */
  public function __construct(
    EntityDisplayRepositoryInterface $entity_display_repository,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
  ) {
    $this->entityDisplayRepository = $entity_display_repository;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityType = 'node';
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_display.repository'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ai_reference_edit_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $node_type = NULL, $field_name = NULL) {
    if (empty($node_type) || empty($field_name)) {
      return $form;
    }

    /** @var \Drupal\Core\Entity\Display\EntityDisplayInterface $form_display_entity */
    $form_display_entity = $this->entityTypeManager
      ->getStorage('entity_form_display')
      ->load("node.{$node_type}.default");

    if (!$form_display_entity) {
      return $form;
    }

    $setting = $form_display_entity->getThirdPartySetting('ai_auto_reference', $field_name);
    if (!is_array($setting) || empty($setting['view_mode'])) {
      return $form;
    }

    $view_modes = $this->entityDisplayRepository->getViewModeOptionsByBundle($this->entityType, $node_type);

    $form['view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('View mode'),
      '#description' => $this->t('View mode that will be used as a source of content for AI analysis.'),
      '#options' => $view_modes,
      '#default_value' => $setting['view_mode'],
      '#required' => TRUE,
    ];

    // Fetch available AI Auto-reference Prompts.
    $prompt_options = [];
    $prompt_entities = $this->entityTypeManager->getStorage('ai_auto_reference_prompt')->loadMultiple();
    foreach ($prompt_entities as $prompt) {
      $prompt_options[$prompt->id()] = $prompt->label();
    }
    $form['prompt'] = [
      '#type' => 'select',
      '#title' => $this->t('Prompt'),
      '#description' => $this->t('Select a predefined AI Auto-reference Prompt'),
      '#options' => $prompt_options,
      '#required' => TRUE,
      '#default_value' => $setting['prompt'],
    ];

    $form['bundle'] = [
      '#type' => 'hidden',
      '#value' => $node_type,
    ];
    $form['field_name'] = [
      '#type' => 'hidden',
      '#value' => $field_name,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save settings'),
      '#weight' => 50,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->cleanValues()->getValues();
    $bundle = $values['bundle'];
    $field_name = $values['field_name'];

    /** @var \Drupal\Core\Entity\Display\EntityDisplayInterface $form_display_entity */
    $form_display_entity = $this->entityTypeManager
      ->getStorage('entity_form_display')
      ->load("{$this->entityType}.{$bundle}.default");
    if (!$form_display_entity) {
      return;
    }

    $form_display_entity->setThirdPartySetting('ai_auto_reference', $field_name, [
      'view_mode' => $values['view_mode'],
      'prompt' => $values['prompt'],
    ]);
    $form_display_entity->save();
    $form_state->setRedirect($this->redirectPath, ['node_type' => $bundle]);
  }

  /**
   * Title for field settings form.
   *
   * @param string $node_type
   *   Node type.
   * @param string $field_name
   *   Field name.
   *
   * @return string
   *   Page title.
   */
  public function pageTitle($node_type, $field_name) {
    $bundle = $this->entityTypeManager->getStorage('node_type')->load($node_type);
    $instances = $this->entityFieldManager->getFieldDefinitions($this->entityType, $node_type);
    if (!isset($instances[$field_name])) {
      return '';
    }
    return $this->t('Edit AI Auto-reference settings for %field field in %bundle content type.', [
      '%field' => $instances[$field_name]->getLabel(),
      '%bundle' => $bundle->label(),
    ]);
  }

}

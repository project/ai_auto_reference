<?php

namespace Drupal\ai_auto_reference\Batch;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Handle the batch processing.
 */
class AiReferenceBatch {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Constructs a new AiReferencesGenerator object.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    RendererInterface $renderer,
    ConfigFactoryInterface $config,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->renderer = $renderer;
    $this->config = $config;
  }

  /**
   * Prepares batch operations for AI autoreference generation.
   *
   * @param \Drupal\node\NodeInterface $node
   *   Node object.
   *
   * @return array
   *   Array with batch operations.
   */
  public static function getbatchSteps(NodeInterface $node): array {
    $operations = [];
    foreach (\Drupal::service('ai_auto_reference.ai_references_generator')->getBundleAiReferencesConfiguration($node->bundle()) as $ai_reference) {
      $operations[] = [
        [__CLASS__, 'batchOperation'],
        [$node, $ai_reference],
      ];
    }
    return $operations;
  }

  /**
   * Batch operation.
   *
   * @param \Drupal\node\NodeInterface $node
   *   Node object.
   * @param array $ai_autoreference
   *   AI autoreference configuration.
   * @param array $context
   *   Batch context.
   */
  public static function batchOperation(
    NodeInterface $node,
    array $ai_autoreference,
    array &$context,
  ) {
    $context['results']['nid'] = $node->id();
    $context['message'] = t('Generation autoreferences for %field.', [
      '%field' => $ai_autoreference['field_name'],
    ]);
    /** @var \Drupal\ai_auto_reference\AiReferenceGenerator $generator */
    $generator = \Drupal::service('ai_auto_reference.ai_references_generator');
    $suggestions = $generator->getAiSuggestions(
      $node,
      $ai_autoreference['field_name'],
      $ai_autoreference['view_mode'],
      $ai_autoreference['prompt'],
    );
    $imploded_suggestions = [];
    foreach ($suggestions as $relevance => $suggestion_ids) {
      $imploded_suggestions[$relevance] = implode(',', $suggestion_ids);
    }
    $context['results']['query'][$ai_autoreference['field_name']] = $imploded_suggestions;
  }

  /**
   * Batch finish callback.
   *
   * @param bool $success
   *   Success flag.
   * @param array $results
   *   Array with results.
   * @param array $operations
   *   Array with operations.
   */
  public static function batchFinished($success, array $results, array $operations) {
    $auto_apply = \Drupal::config('ai_auto_reference.settings')->get('auto_apply_suggestions');
    $auto_apply_relevance_levels = \Drupal::config('ai_auto_reference.settings')->get('auto_apply_relevance_levels');
    $level_mapping = [
      'h' => 'high',
      'm' => 'medium',
    ];

    // Updating node with suggested items.
    if ($auto_apply) {
      if ($node = Node::load($results['nid'])) {
        foreach ($results['query'] as $field_name => $relevancy_values) {
          foreach ($relevancy_values as $level => $relevancy_value_ids) {

            // Check if the auto-apply settings should apply for the
            // given relevance level.
            if (
              !isset($level_mapping[$level])
              || !in_array($level_mapping[$level], $auto_apply_relevance_levels)
            ) {
              continue;
            }

            // If auto-applying, multiple IDs are separated by commas in
            // the response from the AI.
            if ($ids = explode(',', $relevancy_value_ids)) {
              foreach ($ids as $id) {
                if ($id) {
                  $node->{$field_name}[] = ['target_id' => $id];
                }
              }
            }
          }
        }
        $node->save();
      }
    }
    $results['query']['auto-apply'] = $auto_apply;

    // Redirect to node edit page.
    return new RedirectResponse(Url::fromRoute(
      'entity.node.edit_form',
      ['node' => $results['nid']],
      ['query' => $results['query']]
    )->toString());
  }

}
